package stockbit.test.base;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.github.cdimascio.dotenv.Dotenv;
import org.omg.CORBA.TIMEOUT;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import stockbit.test.android_driver.AndroidDriverInstance;
import stockbit.test.utils.Constants;
import stockbit.test.utils.Utils;

import java.util.Objects;

public class BasePageObject {

    Dotenv env = Dotenv.load();

    public AndroidDriver getDriver() {
        return AndroidDriverInstance.androidDriver;
    }

    public AndroidElement waitStrategy(ExpectedCondition<WebElement> conditions) {
        WebDriverWait wait = new WebDriverWait(getDriver(), Constants.TIMEOUT);
        return (AndroidElement) wait.until(conditions);
    }

    public AndroidElement waitUntilClickable(By element) {
        return waitStrategy(ExpectedConditions.elementToBeClickable(element));
    }

    public AndroidElement waitUntilVisible(By element) {
        return waitStrategy(ExpectedConditions.visibilityOfElementLocated(element));
    }

    public By element(String elementLocator) {
        String elementValue = Utils.ELEMENTS.getProperty(elementLocator);
        if (elementValue == null) {
            System.out.println("Couldn't find element : " + elementLocator + " ! Please check properties file!");
            throw new NoSuchElementException("Couldn't find element : " + elementLocator);
        } else {
            String[] locator = elementValue.split("_");
            String locatorType = locator[0];
            String locatorValue = elementValue.substring(elementValue.indexOf("_") + 1);
            switch (locatorType) {
                case "id":
                    if (Objects.equals(env.get("ENV"), "prod")) {
                        return By.id("com.stockbit.android:id/" + locatorValue);
                    } else {
                        return By.id("com.stockbitdev.android:id/" + locatorValue);
                    }
                case "xpath":
                    return By.xpath(locatorValue);
                default:
                    throw new IllegalStateException("Unexpected value: " + locatorType);
            }
        }
    }

    public Integer getExplicitTimeout() {
        return 15;
    }

    public void InputText(String locator, String text) {
        waitUntilVisible(element(locator)).sendKeys(text);
    }

    public void tap(String locator) {
        waitUntilClickable(element(locator)).click();
    }

    public void IsDisplayed(String locator) {
        By element = element(locator);
        try {
            waitUntilVisible(element);
        } catch (TimeoutException error){
            Utils.printError(String.format("Element [%s] not found",element));
        }
    }

    public void isTextSame(String locator, String expectedText) {
        By element = element(locator);
        String actualText = waitUntilVisible(element(locator)).getText();
        if (!actualText.equals(expectedText)) {
            Utils.printError(String.format("Element [%s] witch actual text [%s] not equal [%s]", element, actualText));
        }
    }
}
