package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import stockbit.test.android_driver.AndroidDriverInstance;
import stockbit.test.utils.Constants;

import static stockbit.test.utils.Utils.loadElementProperties;

public class AndroidDriverHooks {
    @Before
    public void initializeAndroidDriver() {
        AndroidDriverInstance.initialize();
        loadElementProperties(Constants.ELEMENTS);
    }

    @After
    public void quitAndroidDriver() {
        AndroidDriverInstance.quit();
    }
}
